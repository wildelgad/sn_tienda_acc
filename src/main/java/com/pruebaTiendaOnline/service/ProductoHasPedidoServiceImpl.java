package com.pruebaTiendaOnline.service;



import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pruebaTiendaOnline.entity.ProductoHasPedido;
import com.pruebaTiendaOnline.repository.ProductoHasPedidoRepository;

@Service
public  class ProductoHasPedidoServiceImpl implements ProductoHasPedidoService {
	
	@Autowired
	private ProductoHasPedidoRepository productoHasPedidoRepository;

	@Override
	@Transactional(readOnly = true)
	public Iterable<ProductoHasPedido> findAll() {
		return productoHasPedidoRepository.findAll();
	}
	@Override
	public Optional<ProductoHasPedido> findById(Long id) {
		return productoHasPedidoRepository.findById(id);
	}

	@Override
	public ProductoHasPedido save(ProductoHasPedido productoHasPedido) {
		return productoHasPedidoRepository.save(productoHasPedido);
	}

	@Override
	public void deleteById(long id) {
		productoHasPedidoRepository.deleteById(id);
		
	}

	
}
	








