package com.pruebaTiendaOnline.service;

import java.util.Optional;

import com.pruebaTiendaOnline.entity.InfoPedido;
import com.pruebaTiendaOnline.entity.Pedido;


public interface PedidoService {
	public Iterable<Pedido> findAll();
	public Optional<Pedido> findById(Long id);
	public String save(InfoPedido infoPedido);
	public void deleteById(long id);

}
