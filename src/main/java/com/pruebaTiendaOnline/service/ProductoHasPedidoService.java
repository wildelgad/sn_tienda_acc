package com.pruebaTiendaOnline.service;

import java.util.Optional;

import com.pruebaTiendaOnline.entity.ProductoHasPedido;

public interface ProductoHasPedidoService {
	public Iterable<ProductoHasPedido> findAll();
	public Optional<ProductoHasPedido> findById(Long id);
	public ProductoHasPedido save( ProductoHasPedido productoHasPedido);
	public void deleteById(long id);

}
