package com.pruebaTiendaOnline.service;


import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pruebaTiendaOnline.entity.Cliente;
import com.pruebaTiendaOnline.repository.ClienteRepository;

@Service
public  class ClienteServiceImpl implements ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;

	@Override
	@Transactional(readOnly = true)
	public Iterable<Cliente> findAll() {
		
		return clienteRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Cliente> findById(Long id) {
		return clienteRepository.findById(id);
	}

	@Override
	@Transactional
	public Cliente save(Cliente cliente) {
		return clienteRepository.save(cliente);
	}

	@Override
	@Transactional
	public void deleteById(long id) {
		clienteRepository.deleteById(id);
		
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Cliente> findCedula(String Cedula) {
		
		return clienteRepository.findByCedula(Cedula);
	}


}
