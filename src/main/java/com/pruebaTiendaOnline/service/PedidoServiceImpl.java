package com.pruebaTiendaOnline.service;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pruebaTiendaOnline.entity.Cliente;
import com.pruebaTiendaOnline.entity.Factura;
import com.pruebaTiendaOnline.entity.InfoPedido;
import com.pruebaTiendaOnline.entity.Pedido;
import com.pruebaTiendaOnline.entity.Producto;
import com.pruebaTiendaOnline.entity.ProductoHasPedido;
import com.pruebaTiendaOnline.repository.ClienteRepository;
import com.pruebaTiendaOnline.repository.FacturaRepository;
import com.pruebaTiendaOnline.repository.PedidoRepository;
import com.pruebaTiendaOnline.repository.ProductoHasPedidoRepository;
import com.pruebaTiendaOnline.repository.ProductoRepository;


@Service
public  class PedidoServiceImpl implements PedidoService {
	
	@Autowired
	private FacturaRepository facturaRepository;
	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private ProductoRepository Producto;
	@Autowired
	private ProductoHasPedidoRepository productoHasPedidoRepository;
	@Autowired
	private PedidoRepository pedidoRepository;

	@Override
	@Transactional(readOnly = true)
	public Iterable<Pedido> findAll() {
		return pedidoRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Pedido> findById(Long id) {
		return pedidoRepository.findById(id);
	}

	@Override
	@Transactional
	public String save(InfoPedido infoPedido) {
		
		//ProductoService Producto= new ProductoServiceImpl();
		//PedidoHasProducto pedidoHasProducto= new PedidoHasPedido();
		ArrayList <Long> IdProductos= infoPedido.getIdProductos();
		ArrayList <Long> CantidadProductos= infoPedido.getCantidadProductos();
		ArrayList<Producto> MisProductos = new ArrayList <Producto>();
		ArrayList<ProductoHasPedido> MisPedidosHasProductos= new ArrayList<ProductoHasPedido>();
		Long PrecioTotal= (long) 0;
		for(int i=0;i<IdProductos.size();i++ ) {
			Producto Miproducto= new Producto();
			Optional <Producto> oProducto = Producto.findById(IdProductos.get(i));
			if(!oProducto.isPresent()) {
				return null;
			
			}
			
			Miproducto= oProducto.get();
			Long precioPedido= Miproducto.getPrecio()*CantidadProductos.get(i);
			PrecioTotal=PrecioTotal+ precioPedido;
			ProductoHasPedido PedidoHasP= new ProductoHasPedido();
			PedidoHasP.setCantidad((CantidadProductos.get(i)).intValue());
			PedidoHasP.setProducto(Miproducto);
			PedidoHasP.setValor_total(precioPedido.intValue());
			MisProductos.add(Miproducto);
			MisPedidosHasProductos.add(PedidoHasP);
		}
		
		Factura MiFactura= new Factura();
		System.out.println("mirando cedula"+ infoPedido.getCedula());
		Optional<Cliente> oCliente = clienteRepository.findByCedula(infoPedido.getCedula());
		if(!oCliente.isPresent()) {
			return null;
		}
		LocalDateTime MiFechaCompra = LocalDateTime.now(); 
		MiFactura.setCliente_idCliente(oCliente.get());
		MiFactura.setFecha_compra(MiFechaCompra);
		MiFactura.setTotal_pagar((int)(PrecioTotal*0.19));
		if(PrecioTotal<100000) {
			MiFactura.setValor_domicilio(10000);
		}
		else {
			MiFactura.setValor_domicilio(0);
		}
		facturaRepository.save(MiFactura);
		
		Pedido MiPedido = new Pedido();
		MiPedido.setFactura_idfactura(MiFactura);
		MiPedido.setFecha_pedido(MiFechaCompra);
		pedidoRepository.save(MiPedido);
		
		
		for(int i=0; i<MisPedidosHasProductos.size(); i++) {
			MisPedidosHasProductos.get(i).setPedido_idPedido(MiPedido);
			System.out.println("dentro del for");
			productoHasPedidoRepository.save(MisPedidosHasProductos.get(i));
			System.out.println("despues de todo");
			
		}
		
		return "Se Guardo tu pedido con exito";
	}

	@Override
	@Transactional
	public void deleteById(long id) {
		pedidoRepository.deleteById(id);
	}


}




