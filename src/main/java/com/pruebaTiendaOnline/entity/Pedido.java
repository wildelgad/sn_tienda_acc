package com.pruebaTiendaOnline.entity;


import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name="pedido")
public class Pedido implements Serializable{

	private static final long serialVersionUID = -5770111941032950978L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	public Long getIdPedido() {
		return id;
	}
	
	@Column(name = "fecha_pedido", nullable = false)
	private LocalDateTime fecha_pedido;
	
	@ManyToOne
	@JoinColumn(name="id_factura")
	private Factura factura_idfactura;

	public LocalDateTime getFecha_pedido() {
		return this.fecha_pedido;
	}

	public void setFecha_pedido(LocalDateTime fecha_pedido) {
		this.fecha_pedido = fecha_pedido;
	}

	public Factura getFactura_idfactura() {
		return this.factura_idfactura;
	}

	public void setFactura_idfactura(Factura factura_idfactura) {
		this.factura_idfactura = factura_idfactura;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

}

