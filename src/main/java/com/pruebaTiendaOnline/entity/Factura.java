package com.pruebaTiendaOnline.entity;



import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="factura")
public class Factura implements Serializable{

	private static final long serialVersionUID = -5770111941032950978L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	public Long getIdFactura() {
		return id;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getValor_domicilio() {
		return valor_domicilio;
	}

	@Column(name = "total_pagar",nullable = false)
	private int  total_pagar;
	
	@Column(name = "valor_domicilio",nullable = false, length = 50)
	private int valor_domicilio;

	@Column(name = "fecha_compra", nullable = false)
	private LocalDateTime fecha_compra;
	
	@ManyToOne
	@JoinColumn(name="idCliente")
	private Cliente cliente_idCliente;

	public int getTotal_pagar() {
		return total_pagar;
	}

	public void setTotal_pagar(int total_pagar) {
		this.total_pagar= total_pagar;
	}

	public int getValor_domicilioo() {
		return valor_domicilio;
	}

	public void setValor_domicilio(int valor_domicilio) {
		this.valor_domicilio = valor_domicilio;
	}

	public LocalDateTime getFecha_compra() {
		return fecha_compra;
	}

	public void setFecha_compra(LocalDateTime fecha_compra) {
		this.fecha_compra = fecha_compra;
	}

	public Cliente getCliente_idCliente() {
		return cliente_idCliente;
	}

	public void setCliente_idCliente(Cliente cliente_idCliente) {
		this.cliente_idCliente = cliente_idCliente;
	}



}


