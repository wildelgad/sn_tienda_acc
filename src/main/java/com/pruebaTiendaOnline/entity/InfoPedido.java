package com.pruebaTiendaOnline.entity;

import java.util.ArrayList;

public class InfoPedido {
	private String Cedula;
	private String Direccion;
	private ArrayList<Long> IdProductos;
	private ArrayList<Long> cantidadProductos;
	
	public ArrayList<Long> getCantidadProductos() {
		return cantidadProductos;
	}
	public void setCantidadProductos(ArrayList<Long> cantidadProductos) {
		this.cantidadProductos = cantidadProductos;
	}
	public String getCedula() {
		return Cedula;
	}
	public void setCedula(String cedula) {
		Cedula = cedula;
	}
	public String getDireccion() {
		return Direccion;
	}
	public void setDireccion(String direccion) {
		Direccion = direccion;
	}
	public ArrayList<Long> getIdProductos() {
		return IdProductos;
	}
	public void setIdProductos(ArrayList<Long> idProductos) {
		IdProductos = idProductos;
	}
	

}
