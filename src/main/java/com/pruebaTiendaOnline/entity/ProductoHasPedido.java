package com.pruebaTiendaOnline.entity;



import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="Producto_has_Pedido")
public class ProductoHasPedido  implements Serializable{

	private static final long serialVersionUID = -5770111941032950978L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	public Long getIdProducto_has_Facturacol() {
		return id;
	}
	
	@Column(name = "cantidad",nullable = false)
	private int  cantidad;
	
	@Column(name = "valor_total",nullable = false, length = 50)
	private int valor_total;
	

	@ManyToOne
	@JoinColumn(name="Pedido_idPedido")
	private Pedido Pedido_idPedido;
	
	@ManyToOne
	@JoinColumn(name="Producto_idProducto")
	private Producto producto;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public int getValor_total() {
		return valor_total;
	}

	public void setValor_total(int valor_total) {
		this.valor_total = valor_total;
	}

	public Pedido getPedido_idPedido() {
		return Pedido_idPedido;
	}

	public void setPedido_idPedido(Pedido pedido_idPedido) {
		Pedido_idPedido = pedido_idPedido;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}




}



