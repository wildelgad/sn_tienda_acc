package com.pruebaTiendaOnline.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.pruebaTiendaOnline.entity.Cliente;
import com.pruebaTiendaOnline.service.ClienteService;


@RestController
@RequestMapping("/api/cliente")
public class ClienteController {
	
	@Autowired
	private ClienteService clienteService;
	
	//Create a new user
	@PostMapping
	public ResponseEntity<?> create (@RequestBody Cliente cliente){
		return ResponseEntity.status(HttpStatus.CREATED).body(clienteService.save(cliente));
	}
	//Read user
	//@GetMapping("/{id}")
	public ResponseEntity<?> read(@PathVariable(value = "id") Long ClienteId){
		Optional<Cliente> oCliente = clienteService.findById(ClienteId);
		if(!oCliente.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(oCliente);
	}
	
	//update an User
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody Cliente clienteDetails, @PathVariable(value = "id") Long clienteId){
		Optional <Cliente> cliente = clienteService.findById(clienteId);
		if(!cliente.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		//BeanUtils.copyProperties(userDetails, user.get()); este modifica todo incluido el id 
		
		cliente.get().setCedula(clienteDetails.getCedula());
		cliente.get().setDireccion(clienteDetails.getDireccion());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(clienteService.save(cliente.get()));
	}
	@GetMapping("/{cedula}")
	public ResponseEntity<?> read(@PathVariable(value = "cedula") String cedula){
		Optional<Cliente> oCliente = clienteService.findCedula(cedula);
		if(!oCliente.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(oCliente);
	}

}
