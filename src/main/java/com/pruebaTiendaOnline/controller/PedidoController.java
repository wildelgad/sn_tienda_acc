package com.pruebaTiendaOnline.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pruebaTiendaOnline.entity.InfoPedido;
import com.pruebaTiendaOnline.service.PedidoService;

@RequestMapping("/api/Pedido")
@RestController
public class PedidoController {
	
	@Autowired
	private PedidoService pedidoService;
	
	@PostMapping
	public ResponseEntity<?> create (@RequestBody InfoPedido infoPedido){
		return ResponseEntity.status(HttpStatus.CREATED).body(pedidoService.save(infoPedido));
	}

}
