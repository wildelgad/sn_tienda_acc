package com.pruebaTiendaOnline.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.pruebaTiendaOnline.entity.Factura;


@Repository
public interface FacturaRepository  extends JpaRepository<Factura, Long> {
	

}
