package com.pruebaTiendaOnline.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.pruebaTiendaOnline.entity.ProductoHasPedido;



@Repository
public interface ProductoHasPedidoRepository extends JpaRepository<ProductoHasPedido, Long> {
	

}